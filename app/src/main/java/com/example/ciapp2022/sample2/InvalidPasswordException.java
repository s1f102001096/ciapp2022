package com.example.ciapp2022.sample2;

public class InvalidPasswordException extends Exception{
    public InvalidPasswordException(String msg){super(msg);}
}
