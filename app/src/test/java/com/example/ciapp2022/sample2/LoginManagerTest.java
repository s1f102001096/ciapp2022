package com.example.ciapp2022.sample2;

import static org.junit.Assert.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.is;

import org.junit.Before;
import org.junit.Test;

public class LoginManagerTest {
    private LoginManager loginManager;

    @Before
    public void setUp() throws ValidateFailedException{
        loginManager = new LoginManager();
        loginManager.register("Testuser1", "Password");
    }

    @Test
    public void testLoginSuccess() throws LoginFailedException,InvalidPasswordException,UserNotFoundException {
        User user = loginManager.login("Testuser1", "Password");
        assertThat(user.getUsername(), is("Testuser1"));
        assertThat(user.getPassword(), is("Password"));
    }


    @Test(expected = InvalidPasswordException.class)
    public void testLoginWrongPassword() throws LoginFailedException,InvalidPasswordException,UserNotFoundException {
        User user = loginManager.login("Testuser1", "1234");
    }

    @Test(expected = UserNotFoundException.class)
    public void testLoginUnregisteredUser() throws LoginFailedException,InvalidPasswordException,UserNotFoundException {
        User user = loginManager.login("Iniad", "Password");
    }
}